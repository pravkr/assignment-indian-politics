package com.politics;

import java.util.Objects;

public class Aircraft extends Vehicle {

	private String name;

	public Aircraft(String name) {
		Objects.requireNonNull(name, "name must not be null");
		if (name.trim().equals("")) {
			throw new IllegalArgumentException("name must not be empty");
		}
		this.name = name;
		System.out.println("Aircraft Created");
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aircraft other = (Aircraft) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Aircraft [name=" + name + "]";
	}

	
	
}
