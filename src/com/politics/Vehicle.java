package com.politics;

import com.politics.spi.VehicleSpec;

public abstract class Vehicle implements VehicleSpec {

	@Override
	public boolean start() {
		System.out.println(getName() + " Engine started");
		return true;
	}
	
	@Override
	public void accelerate(int acceleration) {
		if (acceleration < 0) {
			throw new IllegalArgumentException("acceleration can't be negative");
		}
		System.out.println(getName() + " accelerating at " + acceleration);
	}
	
	@Override
	public boolean stop() {
		System.out.println(getName() + " Engine stopped");
		return true;
	}

	public abstract String getName();
	
	
}
