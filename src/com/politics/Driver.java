package com.politics;

import java.util.List;
import java.util.Objects;

import com.politics.spi.DriverSpec;

public class Driver implements DriverSpec {

	private String name;
	private List<Class<? extends Vehicle>> vehicles;

	public Driver(String name, List<Class<? extends Vehicle>> vehicles) {
		Objects.requireNonNull(name, "name must not be null");
		Objects.requireNonNull(vehicles, "vehicles must not be null");
		if (name.trim().equals("")) {
			throw new IllegalArgumentException("name must not be empty");
		}
		else if (vehicles.isEmpty()) {
			throw new IllegalArgumentException("vehicles must not be empty");
		}
		this.name = name;
		this.vehicles =vehicles;
	}
	
	@Override
	public void drive(Vehicle vehicle) {
		Objects.requireNonNull(vehicle, "vehicle must not be null");
		if (!vehicles.contains(vehicle.getClass())) {
			throw new IllegalArgumentException(getName() + " can't drive " + vehicle);
		}
		System.out.println(getName() + " Driving " + vehicle.getName());
	}

	@Override
	public boolean canDrive(Class<? extends Vehicle> vehicleType) {
		Objects.requireNonNull(vehicleType, "vehicleType must not be null");
		return vehicles
				.parallelStream()
				.anyMatch(v -> v.equals(vehicleType));
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((vehicles == null) ? 0 : vehicles.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Driver other = (Driver) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (vehicles == null) {
			if (other.vehicles != null)
				return false;
		} else if (!vehicles.equals(other.vehicles))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Driver [name=" + name + ", vehicles=" + vehicles + "]";
	}
	
	
	
}
