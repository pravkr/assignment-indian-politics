package com.politics.election;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.politics.DriverVehicleAssignment;
import com.politics.election.IndianPoliticalLeader.IndianPoliticalLeaderBuilder;
import com.politics.spi.Benefit;
import com.politics.spi.Constituency;
import com.politics.spi.PoliticalLeader;
import com.politics.spi.Role;

public class ElectionAuthority {
	
	static final int ONE_CRORE = 10000000;

	private static ElectionAuthority electionAuthority;
	
	private static BenefitProvider benefitProvider = BenefitProvider.instance();
	
	private static Map<Benefit, Supplier<DriverVehicleAssignment>> benefitsRepo;
	
	private static Set<PoliticalLeader> MPS;
	
	private static Set<PoliticalLeader> ministers;
	
	private static PoliticalLeader PM;
	
	static {
		benefitsRepo = new HashMap<>();
		benefitsRepo.put(Benefit.AIRCRAFT, benefitProvider::aircraftBenefit);
		benefitsRepo.put(Benefit.CAR, benefitProvider::carBenefit);
	}
	
	private ElectionAuthority() {
		initialize();
		PoliticalLeader pm2 = getPM();
		Set<PoliticalLeader> ministers2 = getMinisters();
		Set<PoliticalLeader> mps2 = getMPS();
		System.out.println(pm2);
		System.out.println(ministers2);
		System.out.println(mps2);
	}
	
	public static ElectionAuthority instance() {
		if (electionAuthority == null) {
			electionAuthority = new ElectionAuthority();
		}
		return electionAuthority;
	}
	
	private void initialize() {
		MPS = Collections.emptySet();
		ministers = Collections.emptySet();
		PM = null;
	}
	
	public void reset() {
		benefitProvider.resetInventory();
		initialize();
	}
	
	public PoliticalLeader getPM() {
		if (PM == null) {
			PM = getPolLeaderFromFactory(Constituency.VARANASI, Role.PM);
		}
		return PM;
	}
	
	public Set<PoliticalLeader> getMinisters() {
		if (ministers == null || ministers.isEmpty()) {
			ministers = Stream
					.of(getPolLeaderFromFactory(Constituency.BANGALORE, Role.MINISTER),
							getPolLeaderFromFactory(Constituency.MUMBAI, Role.MINISTER))
						.collect(Collectors.toSet());
		}
		return ministers;
	}
	
	public Set<PoliticalLeader> getMPS() {
		if (MPS == null || MPS.isEmpty()) {
			MPS = Stream
					.of(getPolLeaderFromFactory(Constituency.BIHAR, Role.MPS),
							getPolLeaderFromFactory(Constituency.DELHI, Role.MPS),
							getPolLeaderFromFactory(Constituency.KOLKATA, Role.MPS))
						.collect(Collectors.toSet());
		}
		return MPS;
	}

	private PoliticalLeader getPolLeaderFromFactory(Constituency constituency, Role role) {
		return new IndianPoliticalLeaderBuilder()
				.withConstituency(constituency)
				.withRole(role)
				.withBenefit(role
						.getBenefits()
						.stream()
						.map(b -> benefitsRepo.get(b).get())
						.collect(Collectors.toSet()))
				.build();
	}
	
}
