package com.politics.election;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import com.politics.spi.Permission;
import com.politics.spi.PoliticalLeader;

public class ElectionCommisioner {

	private static ElectionCommisioner electionCommisioner;
	
	private ElectionCommisioner() {}
	
	public static ElectionCommisioner instance() {
		if (electionCommisioner == null) {
			electionCommisioner = new ElectionCommisioner();
		}
		return electionCommisioner;
	}
	
	public boolean hasLeaderMadeOffense(PoliticalLeader leader) {
		Objects.requireNonNull(leader, "leader can't be null");
		return leader.exceedsSpendingLimit();
	}
	
	public boolean canArrest(PoliticalLeader leader, Permission permission) {
		Objects.requireNonNull(leader, "leader can't be null");
		Objects.requireNonNull(permission, "permission can't be null");
		if (hasLeaderMadeOffense(leader) && leader.getRole().canBeArrestedWith(permission)) {
			return true;
		}
		return false;
	}
	
	public boolean canArrest(PoliticalLeader leader, Set<Permission> permissions) {
		Objects.requireNonNull(leader, "leader can't be null");
		Objects.requireNonNull(permissions, "permission can't be null");
		if (permissions.isEmpty()) {
			throw new IllegalStateException("permissions cant be empty");
		}
		Optional<Permission> canArrest = permissions
			.parallelStream()
			.filter(p -> canArrest(leader, p))
			.findAny();
		if (canArrest.isPresent()) {
			return true;
		}
		return false;
	}
	
}
