package com.politics.election;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.politics.Aircraft;
import com.politics.Car;
import com.politics.Driver;
import com.politics.DriverVehicleAssignment;
import com.politics.Vehicle;
import com.politics.spi.DriverSpec;

class BenefitProvider {

	private static BenefitProvider benefitProvider;
	private static Map<Availability, Set<DriverVehicleAssignment>> factory;
	
	static {
		initializeInventory();
	}
	
	private BenefitProvider() {}
	
	static BenefitProvider instance() {
		if (benefitProvider == null) {
			benefitProvider = new BenefitProvider();
		}
		return benefitProvider;
	}
	
	static enum Availability {
		RESERVED, UNRESERVED
	}

	/* Inventory */
	private static void initializeInventory() {
		Vehicle car1 = new Car("Swift");
		Vehicle car2 = new Car("Creta");
		Vehicle car3 = new Car("Fortuner");
		Vehicle car4 = new Car("Baleno");
		Vehicle car5 = new Car("Nexon");
		Vehicle car6 = new Car("Altroz");
		Vehicle aircraft = new Aircraft("Indian Airlines");
		DriverSpec akarsh = new Driver("akarsh", Arrays.asList(Car.class));
		DriverSpec pratik = new Driver("pratik", Arrays.asList(Car.class));
		DriverSpec naveen = new Driver("naveen", Arrays.asList(Car.class));
		DriverSpec suraj = new Driver("suraj", Arrays.asList(Car.class));
		DriverSpec anshuman = new Driver("anshuman", Arrays.asList(Car.class));
		DriverSpec jyoti = new Driver("jyoti", Arrays.asList(Car.class, Aircraft.class));
		DriverSpec bilal = new Driver("bilal", Arrays.asList(Car.class, Aircraft.class));
		DriverVehicleAssignment vehAssign1 = new DriverVehicleAssignment(car1, akarsh);
		DriverVehicleAssignment vehAssign2 = new DriverVehicleAssignment(car2, pratik);
		DriverVehicleAssignment vehAssign3 = new DriverVehicleAssignment(car4, naveen);
		DriverVehicleAssignment vehAssign4 = new DriverVehicleAssignment(car5, suraj);
		DriverVehicleAssignment vehAssign5 = new DriverVehicleAssignment(car6, anshuman);
		DriverVehicleAssignment vehAssign6 = new DriverVehicleAssignment(car3, bilal);
		DriverVehicleAssignment vehAssign7 = new DriverVehicleAssignment(aircraft, jyoti);
		factory = new HashMap<>();
		Set<DriverVehicleAssignment> unreservedAssignments = Stream.of(vehAssign1, vehAssign2, vehAssign3, vehAssign4, vehAssign5, vehAssign6, vehAssign7)
				.collect(Collectors.toCollection(HashSet::new));
		factory.put(Availability.UNRESERVED, unreservedAssignments);
		factory.put(Availability.RESERVED, new HashSet<>());
	}
	
	DriverVehicleAssignment carBenefit() {
		return getBenefit(Car.class);
	}
	
	DriverVehicleAssignment aircraftBenefit() {
		return getBenefit(Aircraft.class);
	}
	
	void resetInventory() {
		initializeInventory();
	}
	
	private  DriverVehicleAssignment getBenefit(Class<? extends Vehicle> vehicleTypeToken) {
		Optional<DriverVehicleAssignment> benefit = factory
				.get(Availability.UNRESERVED)
				.parallelStream()
				.filter(dv -> dv.getVehicle().getClass() == vehicleTypeToken)
				.findAny();
			if (benefit.isPresent()) {
				factory.get(Availability.UNRESERVED).remove(benefit.get());
				factory.get(Availability.RESERVED).add(benefit.get());
			}
			else {
				throw new IllegalStateException("Insufficient Resources in Inventory.");
			}
			return benefit.get();
	}
}
