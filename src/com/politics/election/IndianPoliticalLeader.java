package com.politics.election;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import com.politics.DriverVehicleAssignment;
import com.politics.spi.Constituency;
import com.politics.spi.DriverSpec;
import com.politics.spi.PoliticalLeader;
import com.politics.spi.Role;
import com.politics.spi.VehicleSpec;

public class IndianPoliticalLeader implements PoliticalLeader {

	private Constituency constituency;
	private Set<DriverVehicleAssignment> benefits;
	private Role role;
	private List<Spend> spends = new ArrayList<>();
	
	private IndianPoliticalLeader(Constituency constituency, Set<DriverVehicleAssignment> benefits,
			Role role) {
		this.constituency = constituency;
		this.benefits = benefits;
		this.role = role;
	}

	public static class Spend {
		int amount;
		String description;
		
		private Spend(int amount, String description) {
			this.amount = amount;
			this.description = description;
		}
		
		public Spend of(int amount, String description) {
			return new Spend(amount, description);
		}

		public int getAmount() {
			return amount;
		}
		
		public String getDescription() {
			return description;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + amount;
			result = prime * result + ((description == null) ? 0 : description.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Spend other = (Spend) obj;
			if (amount != other.amount)
				return false;
			if (description == null) {
				if (other.description != null)
					return false;
			} else if (!description.equals(other.description))
				return false;
			return true;
		}
		
		
		
	}
	
	public static class IndianPoliticalLeaderBuilder {
		private Constituency constituency;
		private Set<DriverVehicleAssignment> benefits;
		private Role role;
		
		public IndianPoliticalLeaderBuilder withConstituency(Constituency constituency) {
			Objects.requireNonNull(constituency,"constituency can't be null");
			if (this.constituency != null) {
				throw new IllegalStateException("Constituency cannot be assigned multiple times");
			}
			this.constituency = constituency;
			return this;
		}
		
		public IndianPoliticalLeaderBuilder withRole(Role role) {
			Objects.requireNonNull(role, "role can't be null");
			if (this.role != null) {
				throw new IllegalStateException("Role cannot be assigned multiple times");
			}
			this.role = role;
			return this;
		}
		
		public IndianPoliticalLeaderBuilder withBenefit(Set<DriverVehicleAssignment> benefits) {
			Objects.requireNonNull(role, "benefits can't be null");
			if (benefits.isEmpty()) {
				throw new IllegalStateException("Benefits can't be empty");
			}
			this.benefits = benefits;
			return this;
		}
		
		public IndianPoliticalLeader build() {
			Objects.requireNonNull(this.constituency, "constituency can't be null");
			Objects.requireNonNull(this.benefits, "benefits can't be null");
			Objects.requireNonNull(this.role, "role can't be null");
			if (benefits.isEmpty()) {
				throw new IllegalStateException("Driver and Vehicle mapping can't be empty"); // assumption
			}
			return new IndianPoliticalLeader(constituency, benefits, role);
		}
		
	}

	@Override
	public Constituency getConstituency() {
		return constituency;
	}

	@Override
	public Optional<DriverSpec> getDriver(VehicleSpec vehicle) {
		Optional<DriverVehicleAssignment> match = this.benefits
			.parallelStream()
			.filter(dv -> dv.getVehicle().equals(vehicle))
			.findFirst();
		return match.map(DriverVehicleAssignment::getDriver);
	}
	
	@Override
	public Role getRole() {
		return this.role;
	}

	@Override
	public boolean exceedsSpendingLimit() {
		int totalSpend = spends
			.stream()
			.mapToInt(Spend::getAmount)
			.sum();
		return totalSpend > role.getSpendLimit();
	}

	@Override
	public Set<DriverVehicleAssignment> getBenefits() {
		return benefits;
	}

	@Override
	public void spend(int amount, String description) {
		Spend spend = new Spend(amount, description);
		this.spends.add(spend);
	}

	@Override
	public String toString() {
		return "IndianPoliticalLeader [constituency=" + constituency + ", benefits=" + benefits + ", role=" + role
				+ ", spends=" + spends + "]";
	}
	
	
}
