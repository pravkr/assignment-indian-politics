package com.politics.spi;

public enum Constituency {
	/* can be more. it might not be logical as well but kept for simplicity*/
	DELHI, MUMBAI, KOLKATA, BANGALORE, VARANASI, BIHAR;
}
