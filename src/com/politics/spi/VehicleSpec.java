package com.politics.spi;

public interface VehicleSpec {

	boolean start();
	
	void accelerate(int acceleration);
	
	boolean stop();
	
}
