package com.politics.spi;

import java.util.EnumSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum Role {
	
	MPS {
		
		@Override
		public EnumSet<Permission> getPermissions() {
			return withPermission(Permission.VOID);
		}

		@Override
		public EnumSet<Benefit> getBenefits() {
			return withBenefit(Benefit.CAR);
		}

		@Override
		public boolean canBeArrestedWith(Permission permission) {
			if (permission == null) {
				throw new NullPointerException("Permission can't be null");
			}
			return true;
		}

		@Override
		public int getSpendLimit() {
			return ONE_LAKH;
		}
		
	}, 
	
	MINISTER {
		@Override
		public EnumSet<Permission> getPermissions() {
			return withPermission(Permission.VOID);
		}

		@Override
		public EnumSet<Benefit> getBenefits() {
			return withBenefit(Benefit.CAR);
		}

		@Override
		public boolean canBeArrestedWith(Permission permission) {
			if (permission == null) {
				throw new NullPointerException("Permission can't be null");
			}
			return permission == Permission.ARREST_MINISTER;
		}

		@Override
		public int getSpendLimit() {
			return TEN_LAKHS;
		}
	}, 
	
	PM {
		@Override
		public EnumSet<Permission> getPermissions() {
			return withPermission(Permission.ARREST_MINISTER);
		}

		@Override
		public EnumSet<Benefit> getBenefits() {
			return withBenefit(Benefit.CAR, Benefit.AIRCRAFT);
		}

		@Override
		public boolean canBeArrestedWith(Permission permission) {
			if (permission == null) {
				throw new NullPointerException("Permission can't be null");
			}
			return false;
		}

		@Override
		public int getSpendLimit() {
			return ONE_CRORE;
		}
	};
	
	public static final int ONE_CRORE = 10000000;
	public static final int TEN_LAKHS = 1000000;
	public static final int ONE_LAKH = 100000;
	
	public abstract EnumSet<Permission> getPermissions();
	public abstract EnumSet<Benefit> getBenefits();
	public abstract boolean canBeArrestedWith(Permission permission);
	public abstract int getSpendLimit();
	
	private static EnumSet<Permission> withPermission(Permission... permissions) {
		return enumSetOf(Permission.class, permissions);
	}
	
	private static EnumSet<Benefit> withBenefit(Benefit... benefits) {
		return enumSetOf(Benefit.class, benefits);
	}
	
	@SafeVarargs
	private  static <T extends Enum<T>> EnumSet<T> enumSetOf(Class<T> typeToken, T... constants) {
		return Stream
				.of(constants)
				.collect(Collectors
					.toCollection(() -> EnumSet.noneOf(typeToken)));
	}
	
}
