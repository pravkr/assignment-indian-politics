package com.politics.spi;

import java.util.Optional;
import java.util.Set;

import com.politics.DriverVehicleAssignment;

public interface PoliticalLeader {

	Constituency getConstituency();
	Optional<DriverSpec> getDriver(VehicleSpec vehicle);
	Set<DriverVehicleAssignment> getBenefits();
	boolean exceedsSpendingLimit();
	Role getRole();
	void spend(int amount, String description);
	
}
