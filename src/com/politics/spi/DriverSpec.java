package com.politics.spi;

import com.politics.Vehicle;

public interface DriverSpec {

	boolean canDrive(Class<? extends Vehicle> vehicle);
	
	public String getName();
	
	void drive(Vehicle vehicle);
	
}
