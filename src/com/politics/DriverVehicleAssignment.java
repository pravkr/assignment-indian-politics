package com.politics;

import com.politics.spi.DriverSpec;

public class DriverVehicleAssignment {
	private Vehicle vehicle;
	private DriverSpec driver;

	public DriverVehicleAssignment(Vehicle vehicle, DriverSpec driver) {
		this.vehicle = vehicle;
		this.driver = driver;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public DriverSpec getDriver() {
		return driver;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((driver == null) ? 0 : driver.hashCode());
		result = prime * result + ((vehicle == null) ? 0 : vehicle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DriverVehicleAssignment other = (DriverVehicleAssignment) obj;
		if (driver == null) {
			if (other.driver != null)
				return false;
		} else if (!driver.equals(other.driver))
			return false;
		if (vehicle == null) {
			if (other.vehicle != null)
				return false;
		} else if (!vehicle.equals(other.vehicle))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DriverVehicleAssignment [vehicle=" + vehicle + ", driver=" + driver + "]";
	}
	
	

}
