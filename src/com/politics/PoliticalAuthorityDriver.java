package com.politics;

import java.util.EnumSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.politics.election.ElectionAuthority;
import com.politics.election.ElectionCommisioner;
import com.politics.spi.Benefit;
import com.politics.spi.Permission;
import com.politics.spi.PoliticalLeader;
import com.politics.spi.Role;

public class PoliticalAuthorityDriver {

	private static ElectionAuthority electionAuthority = ElectionAuthority.instance();
	private static ElectionCommisioner commisioner = ElectionCommisioner.instance();
	
	/* Test cases designed as per problem statement */
	
	private void PMHaveCarAndAircraftBenefit() {
		System.out.println();
		electionAuthority.reset();
		PoliticalLeader PM = electionAuthority.getPM();
		System.out.println("##TEST - PMHaveCarAndAircraftBenefit##");
		System.out.println("Benefits");
		EnumSet<Benefit> benefitMarkers = PM.getRole().getBenefits();
		benefitMarkers.forEach(System.out::println);
		boolean testPassed = false;
		if (benefitMarkers.contains(Benefit.AIRCRAFT) && benefitMarkers.contains(Benefit.CAR)) {
			testPassed = true;
		}
		Set<Class<? extends Vehicle>> vehicleTypes = PM.getBenefits()
			.stream()
			.map(dv -> dv.getVehicle().getClass())
			.collect(Collectors.toSet());
		if (vehicleTypes.contains(Car.class) && vehicleTypes.contains(Aircraft.class)) {
			testPassed = testPassed && true;
		}
		else {
			testPassed = testPassed && false;
		}
		System.out.println(testPassed ? "TEST PASSED" : "TEST FAILED");
	}
	
	private void MPSHaveOnlyCarBenefit() {
		System.out.println();
		electionAuthority.reset();
		PoliticalLeader MPS = electionAuthority.getMPS().stream().findAny().get();
		System.out.println("##TEST - MPSHaveOnlyCarBenefit##");
		System.out.println("Benefits");
		EnumSet<Benefit> benefitMarkers = MPS.getRole().getBenefits();
		benefitMarkers.forEach(System.out::println);
		boolean testPassed = false;
		if (!benefitMarkers.contains(Benefit.AIRCRAFT) && benefitMarkers.contains(Benefit.CAR)) {
			testPassed = true;
		}
		Set<Class<? extends Vehicle>> vehicleTypes = MPS.getBenefits()
			.stream()
			.map(dv -> dv.getVehicle().getClass())
			.collect(Collectors.toSet());
		if (vehicleTypes.contains(Car.class) && !vehicleTypes.contains(Aircraft.class)) {
			testPassed = testPassed && true;
		}
		else {
			testPassed = testPassed && false;
		}
		System.out.println(testPassed ? "TEST PASSED" : "TEST FAILED");
	}
	
	private void ministersHaveOnlyCarBenefit() {
		System.out.println();
		electionAuthority.reset();
		PoliticalLeader ministers = electionAuthority.getMinisters().stream().findAny().get();
		System.out.println("##TEST - ministersHaveOnlyCarBenefit##");
		System.out.println("Benefits");
		EnumSet<Benefit> benefitMarkers = ministers.getRole().getBenefits();
		benefitMarkers.forEach(System.out::println);
		boolean testPassed = false;
		if (!benefitMarkers.contains(Benefit.AIRCRAFT) && benefitMarkers.contains(Benefit.CAR)) {
			testPassed = true;
		}
		Set<Class<? extends Vehicle>> vehicleTypes = ministers.getBenefits()
			.stream()
			.map(dv -> dv.getVehicle().getClass())
			.collect(Collectors.toSet());
		if (vehicleTypes.contains(Car.class) && !vehicleTypes.contains(Aircraft.class)) {
			testPassed = testPassed && true;
		}
		else {
			testPassed = testPassed && false;
		}
		System.out.println(testPassed ? "TEST PASSED" : "TEST FAILED");
	}
	
	private void PMCanGiveArrestMinisterPermission() {
		System.out.println();
		electionAuthority.reset();
		PoliticalLeader PM = electionAuthority.getPM();
		System.out.println("##TEST - PMCanGiveArrestMinisterPermission##");
		System.out.println("Permissions");
		EnumSet<Permission> permissions = PM.getRole().getPermissions();
		permissions.forEach(System.out::println);
		boolean testPassed = false;
		if (permissions.contains(Permission.ARREST_MINISTER)) {
			testPassed = true;
		}
		System.out.println(testPassed ? "TEST PASSED" : "TEST FAILED");
	}
	
	private void PMHasSpendLimitOfOneCrore() {
		System.out.println();
		electionAuthority.reset();
		PoliticalLeader PM = electionAuthority.getPM();
		System.out.println("##TEST - PMHasSpendLimitOfOneCrore##");
		System.out.println("Spend Limit - " + PM.getRole().getSpendLimit());
		boolean testPassed = false;
		if (PM.getRole().getSpendLimit() == Role.ONE_CRORE) {
			testPassed = true;
		}
		System.out.println(testPassed ? "TEST PASSED" : "TEST FAILED");
	}
	
	private void commisionerCanArrestMPSWithVoidPermissionIfOffenseMade() {
		System.out.println();
		electionAuthority.reset();
		PoliticalLeader MPS = electionAuthority.getMPS().stream().findAny().get();
		System.out.println("##TEST - commisionerCanArrestMPSWithVoidPermissionIfOffenseMade##");
		System.out.println("Spend Limit - " + MPS.getRole().getSpendLimit());
		MPS.spend(Role.MPS.getSpendLimit() + 1, "some spend");
		System.out.println("Spend Exceeded - " + MPS.exceedsSpendingLimit());
		boolean testPassed = false;
		if (commisioner.canArrest(MPS, Permission.VOID)) {
			testPassed = true;
		}
		System.out.println(testPassed ? "TEST PASSED" : "TEST FAILED");
	}
	
	private void commisionerCanArrestMinisterWithPMPermissionIfOffenseMade() {
		System.out.println();
		electionAuthority.reset();
		PoliticalLeader minister = electionAuthority.getMinisters().stream().findAny().get();
		PoliticalLeader PM = electionAuthority.getPM();
		System.out.println("##TEST - commisionerCanArrestMinisterWithPMPermissionIfOffenseMade##");
		System.out.println("Spend Limit - " + minister.getRole().getSpendLimit());
		minister.spend(Role.MINISTER.getSpendLimit() + 1, "some spend");
		System.out.println("Spend Exceeded - " + minister.exceedsSpendingLimit());
		boolean testPassed = false;
		if (commisioner.canArrest(minister, PM.getRole().getPermissions())) {
			testPassed = true;
		}
		System.out.println(testPassed ? "TEST PASSED" : "TEST FAILED");
	}
	
	private void commisionerCannotArrestPMEvenIfOffenseMade() {
		System.out.println();
		electionAuthority.reset();
		PoliticalLeader PM = electionAuthority.getPM();
		System.out.println("##TEST - commisionerCannotArrestPMEvenIfOffenseMade##");
		System.out.println("Spend Limit - " + PM.getRole().getSpendLimit());
		PM.spend(Role.PM.getSpendLimit() + 1, "some spend");
		System.out.println("Spend Exceeded - " + PM.exceedsSpendingLimit());
		boolean testPassed = false;
		if (!commisioner.canArrest(PM, Stream.of(Permission.values()).collect(Collectors.toSet()))) {
			testPassed = true;
		}
		System.out.println(testPassed ? "TEST PASSED" : "TEST FAILED");
	}
	
	private void commisionerCannotArrestAnybodyIfOffenseNotMade() {
		System.out.println();
		electionAuthority.reset();
		PoliticalLeader PM = electionAuthority.getPM();
		PoliticalLeader minister = electionAuthority.getMinisters().stream().findAny().get();
		PoliticalLeader MPS = electionAuthority.getMPS().stream().findAny().get();
		System.out.println("##TEST - commisionerCannotArrestAnybodyIfOffenseNotMade##");
		System.out.println("PM Spend Limit - " + PM.getRole().getSpendLimit());
		System.out.println("Ministers Spend Limit - " + minister.getRole().getSpendLimit());
		System.out.println("MPS Spend Limit - " + MPS.getRole().getSpendLimit());
		System.out.println("PM Spend Exceeded - " + PM.exceedsSpendingLimit());
		System.out.println("Minister Spend Exceeded - " + minister.exceedsSpendingLimit());
		System.out.println("MPS Spend Exceeded - " + MPS.exceedsSpendingLimit());
		boolean testPassed = false;
		if (!(commisioner.canArrest(PM, Stream.of(Permission.values()).collect(Collectors.toSet())) 
				|| commisioner.canArrest(PM, Stream.of(Permission.values()).collect(Collectors.toSet()))
				|| commisioner.canArrest(PM, Stream.of(Permission.values()).collect(Collectors.toSet())))) {
			testPassed = true;
		}
		System.out.println(testPassed ? "TEST PASSED" : "TEST FAILED");
	}
	
	public static void main(String[] args) {
		PoliticalAuthorityDriver politicalAuthorityDriver = new PoliticalAuthorityDriver();
		System.out.println("###TEST START###");
		politicalAuthorityDriver.PMHaveCarAndAircraftBenefit();
		politicalAuthorityDriver.MPSHaveOnlyCarBenefit();
		politicalAuthorityDriver.ministersHaveOnlyCarBenefit();
		politicalAuthorityDriver.PMCanGiveArrestMinisterPermission();
		politicalAuthorityDriver.PMHasSpendLimitOfOneCrore();
		politicalAuthorityDriver.commisionerCanArrestMPSWithVoidPermissionIfOffenseMade();
		politicalAuthorityDriver.commisionerCanArrestMinisterWithPMPermissionIfOffenseMade();
		politicalAuthorityDriver.commisionerCannotArrestPMEvenIfOffenseMade();
		politicalAuthorityDriver.commisionerCannotArrestAnybodyIfOffenseNotMade();
	}

}
